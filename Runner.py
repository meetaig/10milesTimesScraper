class Runner(object):
    def __init__(self,
                 runner_id,
                 rank=None,
                 time=None,
                 finished=False,
                 age=0,
                 nationality="NA",
                 name="Run",
                 lastname="Ner",
                 city="NA",
                 team="NA",
                 pace=0,
                 speed=0,
                 category="",
                 sex="n"):
        super(Runner, self).__init__()
        self.runner_id = runner_id
        self.rank = rank
        self.time = time
        self.finished = finished
        self.age = age
        self.nationality = nationality
        self.name = name
        self.lastname = lastname
        self.city = city
        self.team = team
        self.pace = pace
        self.speed = speed
        self.category = category
        self.sex = sex

        #  5 miles, waasland tunnel in, waaslandtunnel out, finish
        self.checkpoint_times = [None, None, None, self.time]

    def set_checkpoint_time(self, time, idx):
        self.checkpoint_times[idx] = time if time != "-" else 0

    def to_csv_row(self):
        return f"{self.runner_id}," \
               f"{self.name}," \
               f"{self.lastname}," \
               f"{self.time if self.time is not None else 0}," \
               f"{self.rank if self.rank is not None else 0}," \
               f"{self.checkpoint_times[0]}," \
               f"{self.checkpoint_times[1]}," \
               f"{self.checkpoint_times[2]}," \
               f"{self.checkpoint_times[3]}," \
               f"{self.nationality}," \
               f"{1 if self.finished else 0}," \
               f"{self.age}," \
               f"{self.city}," \
               f"{self.pace}," \
               f"{self.speed}," \
               f"{self.category}," \
               f"{self.sex}\n"

    @staticmethod
    def get_csv_header():
        return "runner id,name,lastname,time,rank,checkpt0,checkpt1,checkpt2,checkpt3," \
               "nationality,finished,age,city,team,pace,speed,category,sex\n"

    def print_report(self):
        finish_prefix = "not " if not self.finished else ""
        report_string =  f"Runner # {self.runner_id}\n" \
                         f"name: {self.name} {self.lastname}\n" \
                         f"rank {self.rank}\n" \
                         f"total time: {self.time}\n" \
                         f"Did {finish_prefix}finish the race\n" \
                         f"nationality: {self.nationality}\n"
        print(report_string)

    def __str__(self):
        return f"Runner({self.runner_id}, {self.lastname}, {self.name})"

    def __repr__(self):
        return self.__str__()