"""
:file:      main.py
:brief:     main file for crawling the webform of the antwerp 10 miles website for the
            data of the runners
"""
import os
import re
import requests
import bs4 as BeautifulSoup
from Runner import Runner
from access_website import RequestError
from functools import partial
from toolbox import DataLogger, Logger


def runner_by_id(session, runner_id):
    #event_id = 1188499055219515  # chonorace id of the antwerp 10miles 2018
    event_id = 1187184795226934     # antwerp 10miles 2019
    req_url = f"http://prod.chronorace.be/mypage/mypage.aspx?eventId={event_id}&bibnumber={runner_id}"
    print(f"trying to read {req_url}")

    try:
        # 5 seconds timeout for request, 30 for read
        response = session.get(req_url, timeout=(5, 30))
    except requests.exceptions.ConnectTimeout:
        l = Logger(outdir=os.getcwd(), outfilename="missed_runners.log")
        l.disp(f"{runner_id}")
        raise RequestError(f"Timout for runner id {runner_id}")

    if not response.status_code == 200:
        raise RequestError(f"Website returned status: {response.status_code}")

    doc = BeautifulSoup.BeautifulSoup(response.content, 'lxml')
    # test if we have a successful request
    event_meta = doc.find_all("div", attrs={"id": "event-meta"})
    if len(event_meta) == 0:
        raise RequestError("Form post returned empty form")

    text_h4 = doc.find_all("h4", attrs={"class": "list-group-item-text"})
    if len(text_h4) == 0:
        raise RequestError("Form does not contain expected h4 tags")

    name_raw = text_h4[0].text
    name, lastname = name_raw.split(" ", 1)
    nationality = text_h4[1].text

    # fill in information we already have
    r = Runner(runner_id, name=name, lastname=lastname, nationality=nationality)

    times_table = doc.find_all("table", attrs={"id": "locations-1"})
    try:
        if len(times_table) == 0:
            raise RequestError("Form did not contain a table with the times")
    except RequestError:
        return r

    try:
        r = query_checkpoint(r, doc, "5 Miles - 8.05km", 0)
    except RequestError:
        pass
    try:
        r = query_checkpoint(r, doc, "Ingang Waaslandtunnel ", 1)
    except RequestError:
        pass
    try:
        r = query_checkpoint(r, doc, "Uitgang Waaslandtunnel", 2)
    except RequestError:
        pass
    try:
        r = query_checkpoint(r, doc, "Finish", 3)
    except:
        pass

    return r


def query_checkpoint(runner, doc, checkpoint_string, idx):
    checkpoint_time_table = doc.find_all("tr",
        attrs={"class": "location", "data-name": checkpoint_string})

    if len(checkpoint_time_table) > 1:
        raise RequestError("Found more than one table with finish times...")
    elif len(checkpoint_time_table) < 1:
        raise RequestError(f"No time for checkpoint {checkpoint_string}")

    td_text = checkpoint_time_table[0].findAll("td")

    time_value = td_text[2].text.strip()
    runner.set_checkpoint_time(time_value, idx)
    if idx == 3:
        runner.time = time_value if time_value != "-" else None
        runner.finished = runner.time is not None
        rank_string = td_text[1].contents[0].strip()
        runner.rank = int(rank_string) if rank_string != "-" else None
    return runner

if __name__ == "__main__":
    start_num = 10001
    end_num = 20000
    join_path = os.path.join
    join_path_cwd = partial(join_path, os.getcwd())

    out_path = join_path_cwd("data2019")
    dlogger = DataLogger(outdir=out_path)

    print(f"output directory: {out_path}")

    with requests.session() as session:
        csv_filename = f"runners{start_num:05d}to{end_num:05d}.csv"
        write_header = True
        if os.path.isfile(dlogger.get_current_file_path(csv_filename)):
            write_header = False

        f = dlogger.open_file(csv_filename, "a")

        if write_header:
            f.write(Runner.get_csv_header())
        for runner_id in range(start_num, end_num + 1):
            try:
                r = runner_by_id(session, runner_id)
                r.print_report()

                row = r.to_csv_row()
                f.write(row)
                f.flush()
            except RequestError as e:
                print(e)

        f.close()
