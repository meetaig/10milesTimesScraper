"""
:brief:     script to extract a whole table of 500 runners at once
"""
import os
import requests
from Runner import Runner
import multiprocessing as mp
from toolbox import DataLogger, Logger
from access_website import get_page_doc, RequestError
import bs4 as BeautifulSoup


#EVENT_ID = 1188499055219515  # chonorace id of the antwerp 10miles 2018
EVENT_ID = 1187184795226934     # antwerp 10miles 2019
CLASSEMENT_ID = 19130

CHECKPOINT_5MILES_STRING = "5 Miles - km8.05"
CHECKPOINT_WAASLAND_IN_STRING = "Ingang Waaslandtunnel - km13.3"
CHECKPOINT_WAASLAND_OUT_STRING = "Uitgang Waaslandtunnel - km15.1"
CHECKPOINT_FINISH = "Finish"



def query_table(page_idx, runners_per_page):
    print(f"query_table({page_idx}, {runners_per_page})")

    session = requests.session()
    logger = Logger(outfilename=f"whole_table_extraction_{page_idx}.log")
    data_logger = DataLogger(outdir=logger.outdir, outfilename=logger.outfilename)

    try:
        doc = get_page_doc_local(session, page_idx, logger)
    except RequestError as e:
        logger.disp(f"page {page_idx} RequestError({str(e)})")
    else:
        write_header = True
        start_rank = page_idx * runners_per_page + 1
        end_rank = start_rank + runners_per_page - 1
        data_logger.push_subdir("by_rank")
        runner_file = data_logger.get_current_file_path(f"runners{start_rank}to{end_rank}.csv")

        if os.path.isfile(runner_file):
            write_header = False

        for r_idx in range(runners_per_page):
            try:
                r = get_runner_from_table(r_idx + 1, doc, logger) # indices start at 1 on this page

            except RequestError as e:
                logger.disp(f"Error retriving Runner {r_idx + 1} on page {page_idx} ({str(e)}")
                continue

            # load checkpoint times, which are on a different site
            try:
                r = query_checkpoint_times(session, r)
            except RequestError:
                logger.disp(f"could not retrieve checkpoint times for {r}")
                pass

            with open(runner_file, "a") as f:
                if write_header:
                    f.write(Runner.get_csv_header())
                    write_header = False

                logger.disp(f"writing {r} to csv")
                row = r.to_csv_row()
                f.write(row)
                f.flush()

        data_logger.pop_subdir()


def get_runner_from_table(page_relative_idx, doc, logger):
    results = doc.find_all("tr", attrs={"id": f"ctl00$plcMainZone$rptReport_{page_relative_idx}"})

    if len(results) == 0:
        msg = f"Could not retrieve Runner {page_relative_idx} from current page"
        logger.disp(msg)
        raise RequestError(msg)

    runner_data = results[0].contents # we expect there to be only one of those tables!
    start_number = int(runner_data[2].text.strip())
    rank_raw = runner_data[1].text.strip().replace(".", "")
    rank = int(rank_raw) if (rank_raw != "DNF" and rank_raw != "DSQ") else 0
    finish_time = runner_data[8].text.strip()
    finished = finish_time != ""
    if finish_time == '':
        finish_time = "-"

    age_raw = runner_data[5].text.strip()
    age = int(age_raw) if age_raw != "" else 0

    nationality = runner_data[6].text.strip()

    sex = runner_data[3].text.strip().lower()
    if sex == "":
        sex = "m"
    city = runner_data[13].text.title().replace(",", "")
    lastname, name = runner_data[4].text.rsplit(" ", 1)
    lastname = lastname.replace(",","")
    name = name.replace(",", "")
    category = runner_data[12].text

    pace = runner_data[9].text
    if pace == "":
        pace = "-"

    speed_raw = runner_data[10].text.strip()
    speed = float(speed_raw) if speed_raw != "" else 0

    team = runner_data[7].text

    r = Runner(start_number, rank, finish_time, finished, age, nationality, name, lastname, city,
               team, pace, speed, category, sex)

    return r


def query_checkpoint_times(session, runner):
    runner_id = runner.runner_id
    req_url = f"http://prod.chronorace.be/mypage/mypage.aspx?eventId={EVENT_ID}&bibnumber={runner_id}"
    print(f"trying to read {req_url}")
    try:
        # 5 seconds timeout for request, 30 for read
        response = session.get(req_url, timeout=(5, 30))
    except requests.exceptions.ConnectTimeout:
        l = Logger(outdir=os.getcwd(), outfilename="missed_runners.log")
        l.disp(f"{runner_id}")
        raise RequestError(f"Timout for runner id {runner_id}")

    if not response.status_code == 200:
        raise RequestError(f"Website returned status: {response.status_code}")

    doc = BeautifulSoup.BeautifulSoup(response.content, 'lxml')
    # test if we have a successful request

    times_table = doc.find_all("table", attrs={"id": "locations-1"})
    try:
        if len(times_table) == 0:
            raise RequestError("Form did not contain a table with the times")
    except RequestError:
        return runner

    try:
        runner = query_checkpoint(runner, doc, CHECKPOINT_5MILES_STRING, 0)
    except RequestError:
        pass
    try:
        runner = query_checkpoint(runner, doc, CHECKPOINT_WAASLAND_IN_STRING, 1)
    except RequestError:
        pass
    try:
        runner = query_checkpoint(runner, doc, CHECKPOINT_WAASLAND_OUT_STRING, 2)
    except RequestError:
        pass
    try:
        runner = query_checkpoint(runner, doc, CHECKPOINT_FINISH, 3)
    except:
        pass

    return runner


def query_checkpoint(runner, doc, checkpoint_string, idx):
    checkpoint_time_table = doc.find_all("tr",
        attrs={"class": "location", "data-name": checkpoint_string})

    if len(checkpoint_time_table) > 1:
        raise RequestError("Found more than one table with finish times...")
    elif len(checkpoint_time_table) < 1:
        raise RequestError(f"No time for checkpoint {checkpoint_string}")

    td_text = checkpoint_time_table[0].findAll("td")

    time_value = td_text[2].text.strip()
    runner.set_checkpoint_time(time_value, idx)
    if idx == 3:
        runner.time = time_value if time_value != "-" else None
        runner.finished = runner.time is not None
        rank_string = td_text[1].contents[0].strip()
        runner.rank = int(rank_string) if rank_string != "-" else None
    return runner


def get_page_doc_local(session, page_idx, logger):
    url = f"http://www.chronorace.be/Classements/" \
          f"classement.aspx?eventId={EVENT_ID}&lng=EN&mode=large&" \
          f"IdClassement={CLASSEMENT_ID}&srch=&scope=All&page={page_idx}"
    print(f"trying to read page {url}")

    return get_page_doc(session, url, logger)


def main():
    runners_per_page = 500
    start_page = 1
    end_page = 46

    pool = mp.Pool()
    indices = range(start_page, end_page + 1)
    for idx in indices:
        pool.apply_async(query_table, args=(idx, runners_per_page))

    pool.close()
    pool.join()

if __name__ == "__main__":
    main()