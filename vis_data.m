% visualize data from antwerp 10 miles
function [] = vis_data()

%% user input
clear;
%close all;
base_path = 'by_rank/';

num_bins = 100;
finished_idx = 11;
nationality_idx = 10;
finish_time_idx = 4;
chkpt0_idx = 6;
chkpt3_idx = 9;
age_idx = 12;
sex_idx = 17;


%% loading data 
files = load_with_wildcard(base_path, '*.csv');
data = data_from_files(files);
nationalities_raw = data{:, nationality_idx};
nationalities = unique(nationalities_raw);
num_nationalities = numel(nationalities) - 1; % subtract empty string
disp([num2str(num_nationalities) ' different nationalities']);
disp(nationalities);

[num_runners, ~] = size(data);

%% extract ages
ages = data{:, age_idx};
ages = ages(ages>0);
min_age = min(unique(ages));
max_age = max(unique(ages));

female = 0;
male = 0;
for jj = 1:num_runners
    cur_cell = data{jj, sex_idx};
    is_female = (cur_cell{:} == 'f');
    female = female + is_female;
    male = male + (1-is_female);
end
disp(['Runners from ages ' num2str(min_age) ' to ' num2str(max_age)])

%% extract number of people that finished the race
finished = data{:, finished_idx};
percent_finished = sum(finished) / num_runners;
disp([num2str(sum(finished)) '/' num2str(num_runners)...
    ' runners finished the run (' num2str(percent_finished*100) '%)']);

%% extract times for each checkpoint
time_cell_array = data{:, chkpt0_idx:chkpt3_idx};
run_time = zeros(3, num_runners, 4);
for idx = 1:num_runners
    chk1 = time_cell_array{idx, 1};
    chk2 = time_cell_array{idx, 2};
    chk3 = time_cell_array{idx, 3};
    chk4 = time_cell_array{idx, 4};
    
    hours = [0, 0, 0, 0];
    minutes = [0, 0, 0, 0];
    seconds = [0, 0, 0, 0];
    
    times_array = cell(4, 1);
    times_array{1} = str2double(regexp(chk1, ':', 'split'));
    times_array{2} = str2double(regexp(chk2, ':', 'split'));
    times_array{3} = str2double(regexp(chk3, ':', 'split'));
    times_array{4} = str2double(regexp(chk4, ':', 'split'));
    
    for time_idx = 1:4
        len = sum(times_array{time_idx} ~= 0);
        if len == 1
            seconds(time_idx) = times_array{time_idx}(1);
        end    
        if len == 2
            minutes(time_idx) = times_array{time_idx}(1);
            seconds(time_idx) = times_array{time_idx}(2);
        end
        if len == 3
            seconds(time_idx) = times_array{time_idx}(3);
            minutes(time_idx) = times_array{time_idx}(2);
            hours(time_idx) = times_array{time_idx}(1);
        end        
    end
    seconds(isnan(seconds)) = 0;
    minutes(isnan(minutes)) = 0;
    hours(isnan(hours)) = 0;

    run_time(1, idx, :) = hours;
    run_time(2, idx, :) = minutes;
    run_time(3, idx, :) = seconds;
end

%% now convert to correct time and extract number of people still running
fig1 = figure('name', ['Data of ' num2str(num_runners) ' runners']);
checkpoints = {'Start', '5 Miles', 'Ingang Waaslandtunnel ', 'Uitgang Waaslandtunnel', 'Finish', 'Have finished'};

finishers = zeros(6, 1);
still_running = num_runners;
finishers(1) = num_runners;
finishers(6) = sum(finished);
duration_array = [];
for check_idx = 1:4
    set(0,'CurrentFigure',fig1)
    subplot(4, 4, [(check_idx-1)*4 + 1, (check_idx-1)*4 + 2])
    run_time_duration = duration(run_time(:, :, check_idx)');
    num_before = numel(run_time_duration);
    run_time_duration = run_time_duration(run_time_duration > duration(0, 5, 0));
    num_after = numel(run_time_duration);
        
    still_running = still_running - (num_before-num_after);
    finishers(check_idx + 1) = still_running;    
   
    disp([num2str(finishers(check_idx + 1)) ' still running at ' checkpoints{check_idx + 1}]);
    
    [histo_data, bin_edges] = histcounts(run_time_duration, num_bins);
    histogram('BinEdges', bin_edges, 'BinCounts', histo_data);
    title(checkpoints{check_idx + 1});
    xlim([duration(0, 20, 0), duration(4,10,0)])
    
    min_time = min(run_time_duration);
    max_time = max(run_time_duration);
    disp(['fastest time ' char(min_time)]);
    disp(['slowest time ' char(max_time)]);
end

%% plot number of people measured at a specific checkpoint
subplot(4, 4, 8)
bar_plot = bar(finishers);
ylim([0 1.2*max(finishers)]);
title('Number of people passing a checkpoint');
xticklabels(checkpoints);
xtickangle(20)
for i=1:numel(finishers)
    text(i,finishers(i),num2str(finishers(i),'%d'),...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom')
end

%% plot bar graph of nationalities
subplot(4, 4, [3, 7,11,15])
unique_nations = unique(nationalities);
num_nats = numel(unique_nations);
nat_counts = zeros(num_nats, 1);
for r_idx = 1:num_runners
    found_nations = strfind(unique_nations, nationalities_raw{r_idx});
    nat_idx = find(not(cellfun('isempty', found_nations)));
    nat_counts(nat_idx) = nat_counts(nat_idx) + 1;
end
barh(nat_counts);
for i=1:numel(nat_counts)
    text(nat_counts(i) *1.5, i ,num2str(nat_counts(i),'%d'),...
               'HorizontalAlignment','right',...
               'VerticalAlignment','middle')
end
yticks(1:num_nats);
yticklabels(unique_nations());
ytickangle(0);
title('nations represented');
set(gca, 'XScale', 'log');
ylim([1, num_nats])

%% plot bar graph for male - female
subplot(4, 4, 4)
sexes = [female, male];
bar([1, 2],sexes);
xticklabels({'female', 'male'});
for i=1:2
    text(i,sexes(i),num2str(sexes(i),'%d'),...
               'HorizontalAlignment','center',...
               'VerticalAlignment','bottom')
end

%% plot histogram of ages
subplot(4, 4, [12,16])
num_ages = max_age - min_age;
[ages_hist, ages_bins] = histcounts(ages, num_ages);
histogram('BinEdges', ages_bins, 'BinCounts', ages_hist);
title('age distribution')
xlim([min_age max_age])
view(90, -90);


%% helper functions
    function [files] = load_with_wildcard(base_folder, wildcard)
        filenames = dir(fullfile(base_folder, wildcard));
        filenames = {filenames.name};
        files = cell(1, numel(filenames));
        for file_idx=1:numel(files)
            files(file_idx) = {[base_folder filenames{file_idx}]};
        end
    end

    function [data] = data_from_files(files)
        % as long as the csv data has the same format,the data will be
        % appended
        num_files = numel(files);
        data = table();
        for file_idx=1:numel(files)
            disp(['loading file ' num2str(file_idx) ' ' files{file_idx}])
            tmp_table = readtable(files{file_idx});
            data = [data; tmp_table];
        end
    end
end