import requests
import bs4 as BeautifulSoup

class RequestError(Exception):
    pass


def get_page_doc(session, url, logger=None):
    try:
        # 5 seconds timeout for request, 30 for read
        response = session.get(url, timeout=(5, 30))
    except requests.exceptions.ConnectTimeout:
        if logger is not None:
            logger.error(f"timeout for {url}")
        else:
            raise RequestError(f"timeout for {url}")
    else:
        if not response.status_code == 200:
            raise RequestError(f"Website returned status: {response.status_code}")

        doc = BeautifulSoup.BeautifulSoup(response.content, 'lxml')
        return doc