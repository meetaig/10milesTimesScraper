Scraper tool to download the data of the runners of the Antwerp 10 miles.

Below is the finished visualization of the runners of the 2018 10 miles run.
For example you can see that from 21009 total runners that started the race,
20264 actually finished. But not all of them were correctly measured at the different checkpoints.

What also is very clear, is that the men outweigh the women by roughly 2:1. The median age
range is somewhere between 25-30 years. The bulk of the runners finished the race in a time around
01:30:00.

Of the total number of runners 19282 were Belgian citizens. The others came from 92 different other
countries, when not correcting for the possible doubles in the country abbreviations.

edit: data for 2019 added:
![](img/run2019_10miles.png)


data for the 2018 10 miles:
![](img/full_data_vis.png)
